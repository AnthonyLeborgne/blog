<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\post;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = DB::table('categories')->get();

        // Récupère le nombre de catégorie
        return view('categories/create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|unique:categories'
            
        ],[
            'name.required' => 'Le champs name est requis',
            'name.min' => 'Le champs name doit faire minimum 3 caractères',
            'name.unique' => 'La catégorie excite déjà'
        ]);

        
        $category = new Category();
        
        $category->name = $request->name;
        $category->save();
        return redirect()->back()->with("success",'catégorie ajouté');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('id',$id)->first();
        return view('categories/edit',compact('category'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3|unique:categories'
            
        ],[
            'name.required' => 'Le champs name est requis',
            'name.min' => 'Le champs name doit faire minimum 3 caractères',
            'name.unique' => 'La catégorie excite déjà'
        ]);

        
        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->save();
        return redirect()->back()->with("success",'catégorie modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->destroy($id);
        return redirect()->back()->with("success",'catégorie supprimé');
    }
}
