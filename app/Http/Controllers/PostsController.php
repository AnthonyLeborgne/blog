<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class PostsController extends Controller
{


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = DB::table('categories')->get();
        return view('pages/create',compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:3',
            'filename' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'content' => 'required|min:5'
        ],[
            'title.required' => 'Le champs titre est requis',
            'title.min' => 'Le titre doit faire minimum 3 caractères',
            'content.required' => 'Le champs contenu est requis',
            'content.min' => 'Le titre doit faire minimum 5 caractères',
            'filename.required' => 'Le fichier doit être selectionner',
            'filename.image' => 'les extensions utilisés sont jpeg,png,jpg'     
        ]);
            
        if($file = $request->filename){
            $filename = time().'.'.$file->extension();
            $post = new Post();
            $post->title = $request->title;
            $post->category_id = $request->category;
            $post->content = $request->content;
            $post->users_id = $request->users_id;
            $post->filename = $filename;
            $post->save();

            $file->move(public_path('images'),$filename);
            return redirect()->route('home')->with("success",'article ajouté ');

        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view("pages/show",compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Post::where('id',$id)->first();
        return view('pages/edit',compact('posts'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'title' => 'required|min:3',
            'filename' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'content' => 'required|min:5'
        ],[
            'title.required' => 'Le champs titre est requis',
            'title.min' => 'Le titre doit faire minimum 3 caractères',
            'content.required' => 'Le champs contenu est requis',
            'content.min' => 'Le titre doit faire minimum 5 caractères',
            'filename.required' => 'Le fichier doit être selectionner',
            'filename.image' => 'les extensions utilisés sont jpeg,png,jpg'     
        ]);

        
        if($file = $request->filename){

            $post = Post::findOrFail($id);
            $path = public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR.$post->filename;
            unlink($path);

            $filename = time().'.'.$file->extension();

            $post->title = $request->title;
            $post->category_id = $request->category;
            $post->content = $request->content;
            $post->filename = $filename;
            $post->users_id = $request->users_id;
            $post->save();

            
            $file->move(public_path('images'),$filename);
            return redirect()->route('home')->with("success",'article modifié');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $post = Post::find($id);
       
        $path = public_path().DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR.$post->filename;
        unlink($path);
        $post->destroy($id);
        
        return redirect()->route('home')->with("success",'article supprimé');
    }
}
