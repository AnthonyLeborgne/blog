@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Blog</div>

                <div class="card-body">
                    @foreach($posts as $post)
                    <div class="row">
                        <div class="col">
                            <img src="images/{{ $post->filename }}" alt="{{ $post->title }}" class="rounded " width="300">
                        </div>
                           
                        <div class="col">
                            <h2>{{ $post->title }}</h2>
                            <small>categorie: {{ $post->category->name }} - Auteur: {{ $post->users->name }} - {{ $post->created_at->diffForHumans() }}</small>
                            <p>{{ substr($post->content,0,70).'...' }}</p>
                            @if(Auth::user()->id === $post->users_id)
                                <div class="row">
                                    <div class="col col-sm-4">
                                        <a href="{{ route('edit.post',[$post->id]) }}" class="btn btn-success">Edition</a>
                                    </div>
                                    <div class="col col-sm-4">
                                        <a href="{{ route('show.post',[ $post->id ]) }}" class="btn btn-info">Voir plus</a>
                                    </div>
                                    <div class="col col-sm-4">
                                        <form action="/delete/post/{{ $post->id }}" method="POST" onclick="return confirm('Êtes-vous sur?');">
                                            @method('DELETE')
                                            @csrf
                                            <input type="submit" class="btn btn-danger" value="Supprimer">
                                        </form>
                                    </div> 
                                
                            @else
                                    <div class="offset-md-12">
                                        <a href="{{ url('/show/post',[ $post->id ]) }}" class="btn btn-info">Voir plus</a>
                                    </div>
                            @endif
                                </div>
                            </div>     
                    </div>
                    <hr>
                    @endforeach
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
