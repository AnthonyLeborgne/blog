@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Éditer un article</div>

                <div class="card-body">
                <form method="POST" action="{{ route('edit.post',[$posts->id]) }}"  enctype="multipart/form-data">
                @method('PUT')
                @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('titre') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $posts->content }}" autofocus>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('categorie') }}</label>

                            <div class="col-md-6">
                               
                                <select class="form-control" id="sel1" name="category">
                                        <option value="{{ $posts->category->id }}" selected>{{ $posts->category->name }}</option>
                                </select>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="content" class="col-md-4 col-form-label text-md-right">{{ __('content') }}</label>

                            <div class="col-md-6">
                                <textarea id="content" class="form-control @error('content') is-invalid @enderror" name="content">{{ $posts->content }}</textarea>

                                @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                                <input type="hidden" name="users_id" value="{{ Auth::user()->id }}">
                        </div>

                        <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">{{ __('avatar') }}</label>
                            <div class="col-md-6">
                                <img src="{{ url('images/',[$posts->filename]) }}" alt="" width="300">
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            
                            <label for="filename" class="col-md-4 col-form-label text-md-right">{{ __('photo') }}</label>

                            <div class="col-md-6">
                                <input type="file" name="filename" id="filename" class="form-control btn btn-success">

                                @error('filename')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('modifier') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
