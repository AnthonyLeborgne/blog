@extends('layouts.app')

@section('content')
<img src="{{ url('images/',[$post->filename]) }}" alt="" height="500" width="100%">
<div class="container">
    <h1>{{ $post->title }}</h1> 
    <small>categorie: {{ $post->category->name }} - Auteur: {{ $post->users->name }} - {{ $post->created_at->diffForHumans() }}</small>
    <p>{!! nl2br($post->content) !!}</p>
</div>
@endsection
