<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::middleware('auth')->group(function(){

    // Crud Post
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/show/post/{id}', 'PostsController@show')->name('show.post');

    Route::get('/add/post','PostsController@create')->name('add.post');
    Route::post('/add/post','PostsController@store')->name('add.post');

    Route::get('/edit/post/{id}','PostsController@edit')->name('edit.post');
    Route::put('/edit/post/{id}','PostsController@update')->name('edit.post');

    Route::delete('/delete/post/{id}','PostsController@destroy');

    // Crud Category
    Route::get('/add/category','CategoriesController@create')->name('add.category');
    Route::post('/add/category','CategoriesController@store')->name('add.category');

    Route::get('/edit/category/{id}','CategoriesController@edit')->name('edit.category');
    Route::put('/edit/category/{id}','CategoriesController@update')->name('edit.category');

    Route::delete('/delete/category/{id}','CategoriesController@destroy');

});


